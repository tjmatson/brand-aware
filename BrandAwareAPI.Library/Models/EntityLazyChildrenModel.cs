﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class EntityLazyChildrenModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string sector { get; set; }
        /// <summary>
        /// Return the number of children without loading the children themselves.
        /// </summary>
        public int Children { get; set; }
    }
}
