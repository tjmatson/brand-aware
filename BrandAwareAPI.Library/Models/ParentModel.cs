﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class ParentModel : IEntityModel
    {
        [Description("ParentName")]
        public string EntityName { get; set; }
        [Description("ParentId")]
        public int Id { get; set; }
        [Description("ParentSector")]
        public string Sector { get; set; }
    }
}
