﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class EntityNodeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Sector { get; set; }
        public List<EntityNodeModel> Nodes { get; set; }

        public EntityNodeModel(int id, string name, string sector)
        {
            Id = id;
            Name = name;
            Sector = sector;
            Nodes = new List<EntityNodeModel>();
        }
    }
}
