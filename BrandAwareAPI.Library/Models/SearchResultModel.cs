﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class SearchResultModel
    {
        public List<EntityModel> Results { get; set; }
        public int TotalResults { get; set; }
    }
}
