﻿namespace BrandAwareAPI.Library.Models
{
    public interface IEntityModel
    {
        string EntityName { get; set; }
        int Id { get; set; }
        string Sector { get; set; }
    }
}