﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class QueryModel
    {
        public string Query { get; set; }
        public int Page { get; set; }
        public int Fetch { get; set; }
    }
}
