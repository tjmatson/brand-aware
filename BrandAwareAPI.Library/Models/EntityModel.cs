﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class EntityModel : IEntityModel
    {
        public int Id { get; set; }
        public string EntityName { get; set; }
        public string Sector { get; set; }
    }
}
