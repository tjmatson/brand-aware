﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class ChildParentModel
    {
        public int ChildId { get; set; }
        public string ChildName { get; set; }
        public string ChildSector { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public string ParentSector { get; set; }

        public ChildParentModel()
        {

        }

        public ChildParentModel(int childId, string childName, string childSector, int parentId, string parentName, string parentSector)
        {
            ChildId = childId;
            ChildName = childName;
            ChildSector = childSector;
            
            ParentId = parentId;
            ParentName = parentName;
            ParentSector = parentSector;
        }
    }
}
