﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class RelationshipModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public int ChildId { get; set; }

        public RelationshipModel()
        {
        }

        public RelationshipModel(int parentId, int childId)
        {
            ParentId = parentId;
            ChildId = childId;
        }
    }
}
