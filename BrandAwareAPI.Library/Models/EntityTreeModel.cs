﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class EntityTreeModel
    {
        private EntityNodeModel _root = null;

        public Dictionary<int, EntityNodeModel> Nodes { get; } = new Dictionary<int, EntityNodeModel>();

        public void SetRoot(IEntityModel entity)
        {
            _root = new EntityNodeModel(entity.Id, entity.EntityName, entity.Sector);
            Nodes.Add(entity.Id, _root);
        }

        public EntityNodeModel GetRoot()
        {
            return _root;
        }

        public void AddEntities(IEntityModel root, IEntityModel leaf)
        {
            Nodes.TryGetValue(root.Id, out EntityNodeModel rootNode);
            Nodes.TryGetValue(leaf.Id, out EntityNodeModel leafNode);
            if (rootNode == null)
            {
                rootNode = new EntityNodeModel(root.Id, root.EntityName, root.Sector);
                Nodes.Add(rootNode.Id, rootNode);
            }
            if (leafNode == null)
            {
                leafNode = new EntityNodeModel(leaf.Id, leaf.EntityName, root.Sector);
                Nodes.Add(leafNode.Id, leafNode);
            }
            leafNode.Nodes.Add(rootNode);
        }
    }
}
