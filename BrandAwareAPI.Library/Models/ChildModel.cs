﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace BrandAwareAPI.Library.Models
{
    public class ChildModel : IEntityModel
    {
        [Description("ChildName")]
        public string EntityName { get; set; }
        [Description("ChildId")]
        public int Id { get; set; }
        [Description("ChildSector")]
        public string Sector { get; set; }
    }
}
