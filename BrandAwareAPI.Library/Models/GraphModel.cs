﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Models
{
    public class GraphModel
    {
        public Dictionary<int, IEntityModel> Entities { get; set; }
        public Dictionary<int, List<int>> ChildRelationships { get; set; }
        public Dictionary<int, List<int>> ParentRelationships { get; set; }

        public GraphModel()
        {
            Entities = new Dictionary<int, IEntityModel>();
            ChildRelationships = new Dictionary<int, List<int>>();
            ParentRelationships = new Dictionary<int, List<int>>();
        }

        /// <summary>
        /// Add an entity to the graph. If it already exists in the graph, do nothing.
        /// </summary>
        /// <param name="entity">Entity to add</param>
        public void AddIfNotExists(IEntityModel entity)
        {
            if (!Entities.ContainsKey(entity.Id))
            {
                Entities.Add(entity.Id, entity);
            }
        }

        /// <summary>
        /// Add child and parent entities to graph and create a relationship between the entities.
        /// </summary>
        /// <param name="parent">Parent entity</param>
        /// <param name="child">Child entity</param>
        public void AddRelationship(IEntityModel parent, IEntityModel child)
        {
            AddIfNotExists(parent);
            AddIfNotExists(child);

            CreateRelationship(parent.Id, child.Id);
        }

        /// <summary>
        /// Create a relationship between parent and child entities.
        /// </summary>
        /// <param name="parentId">ID of the parent entity</param>
        /// <param name="childId">ID of the child entity</param>
        public void CreateRelationship(int parentId, int childId)
        {
            // Check if child and parent have adjacency lists, add if not
            if (!ChildRelationships.TryGetValue(parentId, out List<int> childRelationships))
            {
                childRelationships = new List<int>();
                ChildRelationships.Add(parentId, childRelationships);
            }
            childRelationships.Add(childId);

            if (!ParentRelationships.TryGetValue(childId, out List<int> parentRelationships))
            {
                parentRelationships = new List<int>();
                ParentRelationships.Add(childId, parentRelationships);
            }
            parentRelationships.Add(parentId);
        }
    }
}
