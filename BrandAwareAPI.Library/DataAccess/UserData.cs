﻿using BrandAwareAPI.Library.Internal.DataAccess;
using BrandAwareAPI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.DataAccess
{
    public class UserData
    {
        public UserModel GetUserById(string Id)
        {
            SqlDataAccess sql = new SqlDataAccess();

            var parameters = new { Id = Id };

            var output = sql.LoadData<UserModel, dynamic>("dbo.spUserLookup", parameters, "BrandAwareData");
            return output.First();
        }
    }
}
