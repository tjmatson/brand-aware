﻿using BrandAwareAPI.Library.Models;

namespace BrandAwareAPI.Library.DataAccess
{
    public interface IRelationshipData
    {
        void RemoveRelationship(int relationshipId);
        void RemoveRelationship(RelationshipModel relationship);
        void SaveRelationship(RelationshipModel relationship);
        bool CreatesCycle(RelationshipModel relationship);
        void UpdateRelationship(RelationshipModel relationship);
    }
}