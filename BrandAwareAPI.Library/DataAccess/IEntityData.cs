﻿using BrandAwareAPI.Library.Models;
using System.Collections.Generic;

namespace BrandAwareAPI.Library.DataAccess
{
    public interface IEntityData
    {
        EntityModel GetEntity(int id);
        EntityLazyChildrenModel GetEntityLazyChildren(int id);
        List<EntityModel> GetEntities();
        void ChangeName(int id, string name);
        void ChangeSector(int id, string sector);
        SearchResultModel Search(string query, int page, int fetch);
        GraphModel GetEntityLineage(int id);
        EntityNodeModel GetParents(int id);
        EntityNodeModel GetChildren(int id);
        List<EntityLazyChildrenModel> GetDirectChildren(int id);
        void RemoveEntity(int entityId, bool orphanChildren);
        int SaveEntity(EntityModel entity);
        void UpdateEntity(EntityModel entity);
    }
}