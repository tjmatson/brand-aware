﻿using BrandAwareAPI.Library.Internal.DataAccess;
using BrandAwareAPI.Library.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrandAwareAPI.Library.Library;
using AutoMapper.QueryableExtensions;
using Diacritics.Extensions;
using Autofac;

namespace BrandAwareAPI.Library.DataAccess
{
    public class EntityData : IEntityData
    {
        private ISqlDataAccess _sql;
        private IComponentContext _componentContext;

        public EntityData(ISqlDataAccess sql, IComponentContext componentContext)
        {
            _sql = sql;
            _componentContext = componentContext;

            var map = DapperExtensions.GetDescriptionMapper(typeof(ChildModel));
            SqlMapper.SetTypeMap(typeof(ChildModel), map);

            map = DapperExtensions.GetDescriptionMapper(typeof(ParentModel));
            SqlMapper.SetTypeMap(typeof(ParentModel), map);
        }

        public EntityModel GetEntity(int id)
        {
            var output = _sql.LoadData<EntityModel, object>("dbo.spEntity_Lookup", new { Id = id }, "BrandAwareData").First();
            return output;
        }
        public EntityLazyChildrenModel GetEntityLazyChildren(int id)
        {
            var output = _sql.LoadData<EntityLazyChildrenModel, object>("dbo.spEntity_LookupWithChildren", new { Id = id }, "BrandAwareData").First();
            return output;
        }

        public List<EntityModel> GetEntities()
        {
            var output = _sql.LoadData<EntityModel, object>("dbo.spEntity_All", new { }, "BrandAwareData");
            return output;
        }

        /// <summary>
        /// Save an entity to the database.
        /// </summary>
        /// <param name="entity">Entity to save</param>
        /// <returns>ID of the entity in the database</returns>
        public int SaveEntity(EntityModel entity)
        {
            var nameSearch = SearchNormalize(entity.EntityName);
            if (nameSearch == entity.EntityName) nameSearch = null;

            DynamicParameters parameters = new DynamicParameters();

            parameters.Add("Id", dbType: DbType.Int32, direction: ParameterDirection.Output);
            parameters.Add("Name", entity.EntityName, DbType.String, ParameterDirection.Input);
            parameters.Add("NameSearch", nameSearch, DbType.String, ParameterDirection.Input);
            parameters.Add("Sector", entity.Sector, DbType.String, ParameterDirection.Input);

            _sql.SaveData("dbo.spEntity_Insert", parameters, "BrandAwareData");

            var id = parameters.Get<int>("@Id");
            return id;
        }

        public void RemoveEntity(int entityId, bool orphanChildren)
        {
            if (orphanChildren)
            {
                _sql.SaveData("dbo.spEntity_Remove", new { EntityId = entityId }, "BrandAwareData");
                return;
            }

            using (ISqlDataAccess sql = _componentContext.Resolve<ISqlDataAccess>())
            {
                try
                {
                    sql.StartTransaction("BrandAwareData");

                    List<EntityModel> parents = sql.LoadDataInTransaction<EntityModel, object>(
                        "dbo.spEntity_DirectParents", new { Id = entityId });

                    List<EntityModel> children = sql.LoadDataInTransaction<EntityModel, object>(
                        "dbo.spEntity_DirectChildren", new { Id = entityId });

                    sql.SaveDataInTransaction("dbo.spEntity_Remove", new { EntityId = entityId });

                    // Replace the parents of each child with the parents of the deleted entity
                    foreach (var child in children)
                    {
                        foreach (var parent in parents)
                        {
                            sql.SaveDataInTransaction("dbo.spRelationship_Insert",
                                new { ParentId = parent.Id, ChildId = child.Id });
                        }
                    }
                }
                catch
                {
                    sql.RollbackTransaction();
                    throw;
                }
            }
        }

        public void UpdateEntity(EntityModel entity)
        {
            _sql.SaveData("dbo.spEntity_Update", entity, "BrandAwareData");
        }

        public void ChangeName(int id, string name)
        {
            var nameSearch = SearchNormalize(name);
            if (nameSearch == name) nameSearch = null;

            _sql.SaveData("dbo.spEntity_ChangeName", new { Id = id, Name = name, NameSearch = nameSearch }, "BrandAwareData");
        }

        public void ChangeSector(int id, string sector)
        {
            _sql.SaveData("dbo.spEntity_ChangeSector", new { Id = id, Sector = sector }, "BrandAwareData");
        }

        public SearchResultModel Search(string query, int page, int fetch) {
            string storedProcedure;
            object param;
            
            if (string.IsNullOrEmpty(query))
            {
                storedProcedure = "dbo.spEntity_AllPaginated";
                param = new { Page = page, Fetch = fetch };
            }
            else
            {
                storedProcedure = "dbo.spEntity_SearchPaginated";
                param = new { Query = QueryToMultiPrefix(query), Page = page, Fetch = fetch };
            }

            SearchResultModel results = new SearchResultModel();

            string connectionString = _sql.GetConnectionString("BrandAwareData");
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                results.Results = connection.Query<EntityModel, int, EntityModel>(
                    storedProcedure,
                    (entity, totalResults) =>
                    {
                        results.TotalResults = totalResults;
                        return entity;
                    },
                    param: param,
                    commandType: CommandType.StoredProcedure,
                    splitOn: "Results").ToList();
            }

            return results;
        }

        /// <summary>
        /// Get an entity and its parent lineage.
        /// </summary>
        /// <param name="id">ID of the entity</param>
        /// <returns>A hierarchical tree of an entity and its parent lineage</returns>
        public EntityNodeModel GetParents(int id)
        {
            var entity = _sql.LoadData<EntityModel, dynamic>("dbo.spEntityLookupById", new { EntityId = id }, "BrandAwareData").First();
            EntityTreeModel tree = new EntityTreeModel();
            tree.SetRoot(entity);

            string connectionString = _sql.GetConnectionString("BrandAwareData");
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                connection.Query<ParentModel, ChildModel, object>(
                    "dbo.spEntityParents",
                    (parent, child) =>
                    {
                        tree.AddEntities(parent, child);
                        return null;
                    },
                    param: new { EntityId = id },
                    commandType: CommandType.StoredProcedure,
                    splitOn: "ChildId");
            }

            return tree.GetRoot();
        }

        /// <summary>
        /// Get an entity and its child lineage.
        /// </summary>
        /// <param name="id">ID of the entity</param>
        /// <returns>A hierarchical tree of an entity and its child lineage</returns>
        public EntityNodeModel GetChildren(int id)
        {
            //_sql.LoadData<EntityModel, dynamic>("dbo.spEntityParents", new { EntityId = id }, "BrandAwareData");
            var entity = _sql.LoadData<EntityModel, dynamic>("dbo.spEntityLookupById", new { EntityId = id }, "BrandAwareData").First();
            EntityTreeModel tree = new EntityTreeModel();
            tree.SetRoot(entity);

            string connectionString = _sql.GetConnectionString("BrandAwareData");
            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                connection.Query<ChildModel, ParentModel, object>(
                    "dbo.spEntityChildren",
                    (child, parent) =>
                    {
                        tree.AddEntities(child, parent);
                        return null;
                    },
                    param: new { EntityId = id },
                    commandType: CommandType.StoredProcedure,
                    splitOn: "ParentId");
            }

            return tree.GetRoot();
        }

        public List<EntityLazyChildrenModel> GetDirectChildren(int id)
        {
            var output = _sql.LoadData<EntityLazyChildrenModel, object>("dbo.spEntity_DirectChildren", new { Id = id }, "BrandAwareData");
            return output;
        }

        /// <summary>
        /// Get all parents and children of an entity.
        /// </summary>
        /// <param name="id">ID of the entity</param>
        /// <returns>The direct lineage of an entity</returns>
        public GraphModel GetEntityLineage(int id)
        {
            string connectionString = _sql.GetConnectionString("BrandAwareData");

            var graph = new GraphModel();

            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                var entity = _sql.LoadData<EntityModel, dynamic>("dbo.spEntityLookupById", new { EntityId = id }, "BrandAwareData").First();
                graph.AddIfNotExists(entity);

                connection.Query<ParentModel, ChildModel, object>(
                    "dbo.spEntityParents",
                    (parent, child) =>
                    {
                        graph.AddRelationship(parent, child);
                        return null;
                    },
                    param: new { EntityId = id },
                    commandType: CommandType.StoredProcedure,
                    splitOn: "ChildId");

                connection.Query<ChildModel, ParentModel, object>(
                    "dbo.spEntityChildren",
                    (child, parent) =>
                    {
                        graph.AddRelationship(parent, child);
                        return null;
                    },
                    param: new { EntityId = id },
                    commandType: CommandType.StoredProcedure,
                    splitOn: "ParentId");
            }
            return graph;
        }

        public string SearchNormalize(string input)
        {
            var output = input.RemoveDiacritics();

            output = output.Replace("-", " ")
                           .Replace("'", "");

            return output;
        }

        public string QueryToMultiPrefix(string input)
        {
            char[] splitChar = new char[] { ' ' };
            IEnumerable<string> words = input.Split(splitChar, StringSplitOptions.RemoveEmptyEntries);
            words = words.Select(word => $"\"{word}*\"");

            string output = string.Join(" AND ", words);

            return output;
        }
    }
}
