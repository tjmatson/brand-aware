﻿using BrandAwareAPI.Exceptions;
using BrandAwareAPI.Library.Internal.DataAccess;
using BrandAwareAPI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.DataAccess
{
    public class RelationshipData : IRelationshipData
    {
        private ISqlDataAccess _sql;

        public RelationshipData(ISqlDataAccess sql)
        {
            _sql = sql;
        }

        /// <summary>
        /// Checks if creating a relationship between entities would create a cycle in the graph.
        /// </summary>
        /// <param name="relationship">The relationship to create.</param>
        /// <returns>Whether creating the relationship would cause a cycle.</returns>
        public bool CreatesCycle(RelationshipModel relationship)
        {
            // Check if the child entity has the parent entity as a child
            var children = _sql.LoadData<ChildParentModel, object>("dbo.spEntityChildren",
                new { EntityId = relationship.ChildId }, "BrandAwareData");

            if (children.Exists((child) => child.ChildId == relationship.ParentId))
            {
                return true;
            }

            // Check if the child entity has the parent entity as a parent
            var parents = _sql.LoadData<ChildParentModel, object>("dbo.spEntityParents",
                new { EntityId = relationship.ChildId }, "BrandAwareData");

            if (parents.Exists((parent) => parent.ParentId == relationship.ParentId))
            {
                return true;
            }

            return false;
        }

        public void SaveRelationship(RelationshipModel relationship)
        {
            if (CreatesCycle(relationship))
            {
                throw new CycleException("Creating relationship would create cycle or multiple paths from parent to child.");
            }
            _sql.SaveData("dbo.spRelationship_Insert", new { relationship.ChildId, relationship.ParentId }, "BrandAwareData");
        }

        public void RemoveRelationship(int relationshipId)
        {
            _sql.SaveData("dbo.spRelationship_Remove", new { Id = relationshipId }, "BrandAwareData");
        }

        public void UpdateRelationship(RelationshipModel relationship)
        {
            _sql.SaveData("dbo.spRelationship_Update", relationship, "BrandAwareData");
        }

        public void RemoveRelationship(RelationshipModel relationship)
        {
            _sql.SaveData("dbo.spRelationship_RemoveByEntities", new { relationship.ChildId, relationship.ParentId }, "BrandAwareData");
        }
    }
}
