﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.Library
{
    public class DapperExtensions
    {
        public static CustomPropertyTypeMap GetDescriptionMapper(Type classType)
        {
            return new CustomPropertyTypeMap(classType,
                (type, column) => {
                    return type.GetProperties().FirstOrDefault(prop => GetColumnFromDescription(prop) == column);
                }
            );
        }

        private static string GetColumnFromDescription(MemberInfo info)
        {
            if (info == null) return null;

            var attr = (DescriptionAttribute)Attribute.GetCustomAttribute(info, typeof(DescriptionAttribute), false);
            return attr?.Description;
        }

        
    }
}
