﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.DTOs
{
    public class InsertEntityDto
    {
        public string Name { get; set; }
        public string Sector { get; set; }
    }
}
