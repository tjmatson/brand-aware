﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.DTOs
{
    public class RemoveEntityDto
    {
        public int Id { get; set; }
        public bool OrphanChildren { get; set; }
    }
}
