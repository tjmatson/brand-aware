﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.DTOs
{
    public class InsertRelationshipDto
    {
        public int ParentId { get; set; }
        public int ChildId { get; set; }
    }
}
