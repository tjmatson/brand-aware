﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareAPI.Library.DTOs
{
    public class SearchResultDto
    {
        public List<ResponseEntityDto> Results { get; set; }
        public int TotalResults { get; set; }
    }
}
