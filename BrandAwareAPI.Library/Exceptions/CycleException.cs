﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrandAwareAPI.Exceptions
{
    public class CycleException : Exception
    {
        public CycleException()
        {
        }

        public CycleException(string message)
            : base(message)
        {
        }

        public CycleException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}