﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
DECLARE @Value INT;
SELECT @Value = COLUMNPROPERTY(OBJECT_ID('dbo.Entity'), 'NameSearch', 'IsFulltextIndexed')

IF (@Value <> 1)
BEGIN
	PRINT 'Full text indexed'
	CREATE FULLTEXT INDEX ON dbo.Entity(EntityName, NameSearch)
	KEY INDEX PK_Entity ON BrandAwareFT
	WITH CHANGE_TRACKING AUTO
END
GO