﻿CREATE PROCEDURE [dbo].[spEntity_DirectChildren]
	@Id int
AS
	SELECT en.Id, en.EntityName AS [Name], Sector, 
		(SELECT count(*)
		FROM dbo.Entity inEn
		INNER JOIN dbo.Relationship inRe
		ON inEn.Id = inRe.ChildId
		WHERE inRe.ParentId = en.Id) AS Children
	FROM dbo.Entity en
	INNER JOIN dbo.Relationship re
	ON en.Id = re.ChildId
	WHERE re.ParentId = @Id
	ORDER BY en.EntityName;
RETURN 0
