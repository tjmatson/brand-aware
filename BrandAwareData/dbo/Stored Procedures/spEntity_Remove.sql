﻿CREATE PROCEDURE [dbo].[spEntity_Remove]
	@EntityId int
AS
	SET NOCOUNT ON

	DELETE FROM dbo.EntitySector
	WHERE EntityId = @EntityId

	DELETE FROM dbo.Relationship
	WHERE ChildId = @EntityId OR ParentId = @EntityId

	DELETE FROM dbo.Entity
	WHERE Id = @EntityId
RETURN 0
