﻿CREATE PROCEDURE [dbo].[spEntity_LookupWithChildren]
	@Id int
AS
	SELECT en.Id, en.EntityName AS [Name], Sector, 
		(SELECT count(*)
		FROM dbo.Entity inEn
		INNER JOIN dbo.Relationship inRe
		ON inEn.Id = inRe.ChildId
		WHERE inRe.ParentId = en.Id) AS Children
	FROM dbo.Entity en
	WHERE en.Id = @Id
RETURN 0
