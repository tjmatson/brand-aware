﻿CREATE PROCEDURE [dbo].[spEntity_SearchPaginated]
	@Query nvarchar(512),
	@Page int,
	@Fetch int
AS
	WITH ResultsCTE AS (
		SELECT Id, EntityName, Sector
		FROM dbo.Entity
		WHERE CONTAINS(EntityName, @Query) OR CONTAINS(NameSearch, @Query)
	),
	CountCTE AS (
		SELECT COUNT(*) AS Results FROM ResultsCTE
	)

	SELECT *
	FROM ResultsCTE
	CROSS JOIN CountCTE
	ORDER BY EntityName
	OFFSET (@Page - 1) * @Fetch ROWS
	FETCH NEXT @Fetch ROWS ONLY
RETURN 0