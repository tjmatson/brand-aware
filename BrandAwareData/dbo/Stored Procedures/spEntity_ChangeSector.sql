﻿CREATE PROCEDURE [dbo].[spEntity_ChangeSector]
	@Id int,
	@Sector nvarchar(128)
AS
	UPDATE dbo.Entity
	SET Sector = @Sector
	WHERE Id = @Id
RETURN 0
