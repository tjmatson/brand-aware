﻿CREATE PROCEDURE [dbo].[spEntity_Lookup]
	@Id int
AS
	SET NOCOUNT ON;

	SELECT Id, EntityName, Sector
	FROM dbo.Entity
	WHERE Id = @Id
RETURN 0
