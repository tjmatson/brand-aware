﻿CREATE PROCEDURE [dbo].[spEntity_All]
AS
	SELECT Id, EntityName, Sector
	FROM dbo.Entity
	ORDER BY EntityName;
RETURN 0
