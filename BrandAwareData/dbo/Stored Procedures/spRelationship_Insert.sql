﻿CREATE PROCEDURE [dbo].[spRelationship_Insert]
	@ParentId int,
	@ChildId int
AS
	SET NOCOUNT ON

	INSERT INTO dbo.Relationship (ParentId, ChildId)
	VALUES (@ParentId, @ChildId)
RETURN 0
