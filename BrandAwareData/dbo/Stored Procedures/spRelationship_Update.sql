﻿CREATE PROCEDURE [dbo].[spRelationship_Update]
	@Id int,
	@ChildId int,
	@ParentId int
AS
	SET NOCOUNT ON

	UPDATE dbo.Relationship
	SET ChildId = @ChildId, ParentId = @ParentId
	WHERE Id = @Id
RETURN 0
