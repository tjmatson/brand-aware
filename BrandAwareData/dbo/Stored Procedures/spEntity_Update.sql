﻿CREATE PROCEDURE [dbo].[spEntity_Update]
	@Id int,
	@EntityName nvarchar(128)
AS
	SET NOCOUNT ON

	UPDATE dbo.Entity
	SET EntityName = @EntityName
	WHERE Id = @Id
RETURN 0
