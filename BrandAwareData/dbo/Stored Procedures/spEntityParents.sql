﻿CREATE PROCEDURE [dbo].[spEntityParents]
	@EntityId int
AS
	WITH entity_structure AS (
		SELECT	en.[Id] AS ParentId,
				en.EntityName AS ParentName,
				en.Sector AS ParentSector,
				CAST(NULL AS INT) AS ChildId,
				CAST(NULL AS NVARCHAR(128)) AS ChildName,
				CAST(NULL AS NVARCHAR(128)) AS ChildSector
		FROM dbo.Entity en
		WHERE en.[Id] = @EntityId

		UNION ALL

		SELECT	en.[Id] AS ParentId,
				en.EntityName AS ParentName,
				en.Sector AS ParentSector,
				entity_structure.ParentId AS ChildId,
				entity_structure.ParentName AS ChildName,
				entity_structure.ParentSector AS ChildSector
		FROM dbo.Entity en
		INNER JOIN dbo.Relationship re
			ON en.[Id] = re.ParentId
		INNER JOIN entity_structure
			ON re.ChildId = entity_structure.ParentId
	)
	SELECT DISTINCT ParentId, ParentName, ParentSector, ChildId, ChildName, ChildSector
	FROM entity_structure
	WHERE ChildId IS NOT NULL
	ORDER BY ParentName;
RETURN 0;
