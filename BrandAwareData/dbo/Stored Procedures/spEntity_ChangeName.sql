﻿CREATE PROCEDURE [dbo].[spEntity_ChangeName]
	@Id int,
	@Name nvarchar(128),
	@NameSearch nvarchar(128)
AS
	UPDATE dbo.Entity
	SET EntityName = @Name, NameSearch = @NameSearch
	WHERE Id = @Id
RETURN 0
