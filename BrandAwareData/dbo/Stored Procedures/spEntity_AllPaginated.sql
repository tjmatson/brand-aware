﻿CREATE PROCEDURE [dbo].[spEntity_AllPaginated]
	@Page int,
	@Fetch int
AS
	WITH ResultsCTE AS (
		SELECT Id, EntityName, Sector
		FROM dbo.Entity
	),
	CountCTE AS (
		SELECT COUNT(*) AS Results FROM ResultsCTE
	)

	SELECT *
	FROM dbo.Entity
	CROSS JOIN CountCTE
	ORDER BY EntityName
	OFFSET (@Page - 1) * @Fetch ROWS
	FETCH NEXT @Fetch ROWS ONLY
RETURN 0
