﻿CREATE PROCEDURE [dbo].[spEntityChildren]
	@EntityId int
AS
	WITH entity_structure AS (
		SELECT	en.[Id] AS ChildId,
				en.EntityName AS ChildName,
				en.Sector as ChildSector,
				CAST(NULL AS INT) AS ParentId,
				CAST(NULL AS NVARCHAR(128)) AS ParentName,
				CAST(NULL AS NVARCHAR(128)) AS ParentSector
		FROM dbo.Entity en
		WHERE en.[Id] = @EntityId

		UNION ALL

		SELECT	en.[Id] AS ChildId,
				en.EntityName AS ChildName,
				en.Sector as ChildSector,
				entity_structure.ChildId AS ParentId,
				entity_structure.ChildName AS ParentName,
				entity_structure.ChildSector AS ParentSector
		FROM dbo.Entity en
		INNER JOIN dbo.Relationship re
			ON en.[Id] = re.ChildId
		INNER JOIN entity_structure
			ON re.ParentId = entity_structure.ChildId
	)
	SELECT DISTINCT ChildId, ChildName, ChildSector, ParentId, ParentName, ParentSector
	FROM entity_structure
	WHERE ParentId IS NOT NULL
	ORDER BY ChildName;
RETURN 0;
