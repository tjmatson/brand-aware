﻿CREATE PROCEDURE [dbo].[spRelationship_Remove]
	@Id int
AS
	SET NOCOUNT ON

	DELETE FROM dbo.Relationship
	WHERE Id = @Id
RETURN 0
