﻿CREATE PROCEDURE [dbo].[spRelationship_RemoveByEntities]
	@ChildId int,
	@ParentId int
AS
	SET NOCOUNT ON

	DELETE FROM dbo.Relationship
	WHERE ChildId = @ChildId AND ParentId = @ParentId
RETURN 0
