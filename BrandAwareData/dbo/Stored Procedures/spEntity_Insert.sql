﻿CREATE PROCEDURE [dbo].[spEntity_Insert]
	@Id int output,
	@Name nvarchar(128),
	@NameSearch nvarchar(128),
	@Sector nvarchar(128)
AS
	SET NOCOUNT ON

	INSERT INTO dbo.Entity (EntityName, NameSearch, Sector)
	VALUES (@Name, @NameSearch, @Sector)

	SELECT @Id = @@IDENTITY
RETURN 0
