﻿CREATE PROCEDURE [dbo].[spEntity_DirectParents]
	@Id int
AS
	SELECT en.Id, en.EntityName AS [Name], Sector
	FROM dbo.Entity en
	INNER JOIN dbo.Relationship re
	ON en.Id = re.ParentId
	WHERE re.ChildId = @Id
	ORDER BY en.EntityName;
RETURN 0