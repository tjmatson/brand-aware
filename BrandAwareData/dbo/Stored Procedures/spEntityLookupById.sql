﻿CREATE PROCEDURE [dbo].[spEntityLookupById]
	@EntityId int
AS
	SET NOCOUNT ON;

	SELECT Id, EntityName, Sector
	FROM dbo.Entity
	WHERE Id = @EntityId;
RETURN 0
