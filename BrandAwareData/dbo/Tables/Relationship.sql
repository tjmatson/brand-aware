﻿CREATE TABLE [dbo].[Relationship]
(
	[Id] INT NOT NULL IDENTITY, 
    [ParentId] INT NOT NULL, 
    [ChildId] INT NOT NULL, 
    CONSTRAINT [FK_Relationship_ToParentEntity] FOREIGN KEY (ParentId) REFERENCES Entity(Id), 
    CONSTRAINT [FK_Relationship_ToChildEntity] FOREIGN KEY (ChildId) REFERENCES Entity(Id),
    CONSTRAINT PK_Relationship PRIMARY KEY ([Id])
)
