﻿CREATE TABLE [dbo].[EntitySector]
(
	[Id] INT NOT NULL IDENTITY,
	[EntityId] INT NOT NULL,
	[SectorId] INT NOT NULL, 
    CONSTRAINT [FK_EntitySector_ToEntity] FOREIGN KEY (EntityId) REFERENCES Entity(Id), 
    CONSTRAINT [FK_EntitySector_ToSector] FOREIGN KEY (SectorId) REFERENCES Sector(Id),
	CONSTRAINT PK_EntitySector PRIMARY KEY ([Id])
)
