﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BrandAwareAPI.Helpers
{
    public class Config
    {
        public static int GetMaxSearchResults()
        {
            string attr = ConfigurationManager.AppSettings["MaxSearchResults"];
            bool isValid = int.TryParse(attr, out int maxSearchResults);

            if (!isValid)
            {
                throw new ConfigurationErrorsException("Max search result configuration invalid");
            }

            return maxSearchResults;
        }
    }
}