﻿using AutoMapper;
using BrandAwareAPI.Exceptions;
using BrandAwareAPI.Library.DataAccess;
using BrandAwareAPI.Library.DTOs;
using BrandAwareAPI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BrandAwareAPI.Controllers
{
    [Authorize]
    public class RelationshipController : ApiController
    {
        private IRelationshipData _relationshipData;
        private IMapper _mapper;

        public RelationshipController(IRelationshipData relationshipData, IMapper mapper)
        {
            _relationshipData = relationshipData;
            _mapper = mapper;
        }

        [HttpPost]
        [Authorize(Roles = "Contributor")]
        public HttpResponseMessage Post(InsertRelationshipDto relationshipDto)
        {
            try
            {
                var relationship = _mapper.Map<RelationshipModel>(relationshipDto);
                _relationshipData.SaveRelationship(relationship);
            } catch(CycleException e)
            {
                HttpError error = new HttpError(e.Message);
                return Request.CreateResponse(HttpStatusCode.Conflict, error);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        [Authorize(Roles = "Contributor")]
        public void Delete(int id)
        {
            _relationshipData.RemoveRelationship(id);
        }

        [HttpPut]
        [Authorize(Roles = "Contributor")]
        public void Put(InsertRelationshipDto relationshipDto)
        {
            var relationship = _mapper.Map<RelationshipModel>(relationshipDto);
            _relationshipData.UpdateRelationship(relationship);
        }

        [HttpPost]
        [Route("api/relationship/DeleteByEntities")]
        [Authorize(Roles = "Contributor")]
        public void DeleteByEntities(InsertRelationshipDto relationshipDto)
        {
            var relationship = _mapper.Map<RelationshipModel>(relationshipDto);
            _relationshipData.RemoveRelationship(relationship);
        }
    }
}