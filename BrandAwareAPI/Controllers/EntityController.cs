﻿using System.Configuration;
using AutoMapper;
using BrandAwareAPI.Exceptions;
using BrandAwareAPI.Library.DataAccess;
using BrandAwareAPI.Library.DTOs;
using BrandAwareAPI.Library.Internal.DataAccess;
using BrandAwareAPI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BrandAwareAPI.Helpers;

namespace BrandAwareAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/Entity")]
    public class EntityController : ApiController
    {
        private IEntityData _entityData;
        private IRelationshipData _relationshipData;
        private IMapper _mapper;
        private int _maxSearchResults;

        public EntityController(IEntityData entityData, IRelationshipData relationshipData, IMapper mapper)
        {
            _entityData = entityData;
            _relationshipData = relationshipData;
            _mapper = mapper;
            _maxSearchResults = Config.GetMaxSearchResults();
        }

        /// <summary>
        /// Get a list of every entitity in the database.
        /// </summary>
        /// <returns>All entities.</returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("GetAll")]
        public List<ResponseEntityDto> GetAll()
        {
            var entityList = _entityData.GetEntities();
            var entities = _mapper.Map<List<ResponseEntityDto>>(entityList);
            return entities;
        }

        /// <summary>
        /// Get a specific entity.
        /// </summary>
        /// <param name="id">ID of the entity to get.</param>
        /// <returns>Entity with a specific ID.</returns>
        [AllowAnonymous]
        [HttpGet]
        public ResponseEntityDto Get(int id)
        {
            var entity = _mapper.Map<ResponseEntityDto>(_entityData.GetEntity(id));
            return entity;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Search")]
        public SearchResultDto Search(QueryModel query)
        {
            // Limit number of results allowed
            query.Fetch = Math.Min(query.Fetch, _maxSearchResults);

            var searchResults = _entityData.Search(query.Query, query.Page, query.Fetch);
            var results = _mapper.Map<SearchResultDto>(searchResults);

            return results;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("LazyLoad/{id}")]
        public EntityLazyChildrenModel LazyLoad(int id)
        {
            var entity = _entityData.GetEntityLazyChildren(id);
            return entity;
        }

        /// <summary>
        /// Add an entity to the database.
        /// </summary>
        /// <param name="entityDto">Data associated with the entity.</param>
        /// <returns>The ID of the entity in the database.</returns>
        [HttpPost]
        [Authorize(Roles = "Contributor")]
        public int Post(InsertEntityDto entityDto)
        {
            var entity = _mapper.Map<EntityModel>(entityDto);
            var id = _entityData.SaveEntity(entity);
            return id;
        }

        /// <summary>
        /// Remove an entity and its associated data.
        /// </summary>
        /// <param name="entityInfo"></param>
        [HttpPost]
        [Route("DeleteEntity")]
        [Authorize(Roles = "Contributor")]
        public void Delete(RemoveEntityDto entityInfo)
        {
            _entityData.RemoveEntity(entityInfo.Id, entityInfo.OrphanChildren);
        }

        /// <summary>
        /// Change an entity's name.
        /// </summary>
        /// <param name="id">ID of the entity to modify.</param>
        /// <param name="name">The new name of the entity.</param>
        [HttpPost]
        [Route("ChangeName/{id}")]
        [Authorize(Roles = "Contributor")]
        public HttpResponseMessage ChangeName(int id, [FromBody] string name)
        {
            if (name.Length == 0)
            {
                HttpError error = new HttpError("Name can not be empty.");
                return Request.CreateResponse(HttpStatusCode.BadRequest, error);
            }
            _entityData.ChangeName(id, name);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("ChangeSector/{id}")]
        [Authorize(Roles = "Contributor")]
        public void ChangeSector(int id, [FromBody] string sector)
        {
            _entityData.ChangeSector(id, sector);
        }

        /// <summary>
        /// Create a new child entity and add it to an existing entity.
        /// </summary>
        /// <param name="id">The entity to add the child to.</param>
        /// <param name="childDto">The child to create.</param>
        /// <returns>The ID of the new child entity</returns>
        [HttpPost]
        [Route("CreateChild/{id}")]
        [Authorize(Roles = "Contributor")]
        public int CreateChild(int id, InsertEntityDto childDto)
        {
            var child = _mapper.Map<EntityModel>(childDto);
            int childId = _entityData.SaveEntity(child);

            RelationshipModel relationship = new RelationshipModel(id, childId);
            _relationshipData.SaveRelationship(relationship);

            return childId;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("Parents/{id}")]
        public EntityNodeModel GetParents(int id)
        {
            return _entityData.GetParents(id);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("Children/{id}")]
        public EntityNodeModel GetChildren(int id)
        {
            return _entityData.GetChildren(id);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("DirectChildren/{id}")]
        public List<EntityLazyChildrenModel> GetDirectChildren(int id)
        {
            return _entityData.GetDirectChildren(id);
        }

        /// <summary>
        /// Get the parents and children of an entity.
        /// 
        /// All of the entities are returned as name/value pairs with their ID as the key.
        /// Relationships between the entities are returned as an adjacency list.
        /// The parent and child relationships are returned as separate adjacency lists.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Entity parents and children.</returns>
        [AllowAnonymous]
        [HttpGet]
        [Route("Lineage/{id}")]
        public GraphModel GetStructure(int id)
        {
            return _entityData.GetEntityLineage(id);
        }
    }
}
