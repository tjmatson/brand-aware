﻿using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using BrandAwareAPI.Library.DataAccess;
using BrandAwareAPI.Library.DTOs;
using BrandAwareAPI.Library.Internal.DataAccess;
using BrandAwareAPI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;

namespace BrandAwareAPI
{
    public class AutofacConfig
    {
        public static void RegisterAutofac()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            var automapConfig = new MapperConfiguration(cfg => {
                cfg.CreateMap<InsertEntityDto, EntityModel>()
                    .ForMember(dest => dest.EntityName, opt => opt.MapFrom(src => src.Name));

                cfg.CreateMap<EntityModel, ResponseEntityDto>()
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.EntityName));

                cfg.CreateMap<InsertRelationshipDto, RelationshipModel>();

                cfg.CreateMap<SearchResultModel, SearchResultDto> ();
            });

            var mapper = automapConfig.CreateMapper();

            builder.RegisterInstance(mapper);

            builder.RegisterType<SqlDataAccess>().As<ISqlDataAccess>();
            builder.RegisterType<EntityData>().As<IEntityData>();
            builder.RegisterType<RelationshipData>().As<IRelationshipData>();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}