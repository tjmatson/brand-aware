﻿using BrandAwareDesktopUI.Library.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.Models
{
    public class EntityItemModel
    {
        public EntityItemModel(EntityModel entity)
        {
            Entities = new ObservableCollection<EntityItemModel>();
            Entity = new EntityModel();
            Entity.Id = entity.Id;
            Entity.EntityName = entity.EntityName;
        }

        public EntityModel Entity { get; set; }
        public ObservableCollection<EntityItemModel> Entities { get; set; }
    }
}
