﻿using BrandAwareDesktopUI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.EventModels
{
    public class AddChildEvent
    {
        public EntityModel Parent { get; set; }

        public AddChildEvent(EntityModel parent)
        {
            Parent = parent;
        }
    }
}
