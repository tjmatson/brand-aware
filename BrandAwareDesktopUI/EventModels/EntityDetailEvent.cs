﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.EventModels
{
    public class EntityDetailEvent
    {
        public int Id { get; set; }

        public EntityDetailEvent(int id)
        {
            Id = id;
        }
    }
}
