﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.EventModels
{
    public class ChangeViewEvent
    {
        public readonly Type ViewModelType;

        public ChangeViewEvent(Type viewModelType)
        {
            ViewModelType = viewModelType;
        }
    }
}
