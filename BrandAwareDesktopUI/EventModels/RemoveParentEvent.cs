﻿using BrandAwareDesktopUI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.EventModels
{
    public class RemoveParentEvent
    {
        public List<EntityModel> Parents { get; set; }
        public EntityModel Entity { get; set; }

        public RemoveParentEvent(EntityModel entity, List<EntityModel> parents)
        {
            Entity = entity;
            Parents = parents;
        }
    }
}
