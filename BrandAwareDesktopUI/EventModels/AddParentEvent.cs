﻿using BrandAwareDesktopUI.Library.Models;

namespace BrandAwareDesktopUI.EventModels
{
    public class AddParentEvent
    {
        public EntityModel ChildEntity { get; set; }

        public AddParentEvent(EntityModel childEntity)
        {
            ChildEntity = childEntity;
        }
    }
}
