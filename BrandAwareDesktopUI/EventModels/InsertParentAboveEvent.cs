﻿using BrandAwareDesktopUI.Library.Models;
using System;
using System.Collections.Generic;

namespace BrandAwareDesktopUI.EventModels
{
    public class InsertParentAboveEvent
    {
        public EntityModel ChildEntity { get; set; }
        public List<EntityModel> ParentEntities { get; set; }

        public InsertParentAboveEvent(EntityModel childEntity, List<EntityModel> parentEntities)
        {
            ChildEntity = childEntity;
            ParentEntities = parentEntities;
        }
    }
}
