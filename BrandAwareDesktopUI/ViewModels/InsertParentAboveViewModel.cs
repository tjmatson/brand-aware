﻿using BrandAwareDesktopUI.EventModels;
using BrandAwareDesktopUI.Library.Api;
using BrandAwareDesktopUI.Library.Models;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.ViewModels
{
    public class InsertParentAboveViewModel : Screen, IHandle<InsertParentAboveEvent>
    {
        private IEntityEndpoint _entityEndpoint;
        private IRelationshipEndpoint _relationshipEndpoint;
        private IEventAggregator _events;
        private EntityComboBoxViewModel _entityComboBox;
        private EntityModel _childEntity;
        private HashSet<EntityModel> _checkedParents;
        private BindingList<EntityModel> _parentEntities;

        public InsertParentAboveViewModel(IEntityEndpoint entityEndpoint, IRelationshipEndpoint relationshipEndpoint, IEventAggregator events)
        {
            _entityEndpoint = entityEndpoint;
            _relationshipEndpoint = relationshipEndpoint;
            _events = events;

            _events.Subscribe(this);
            ParentEntityComboBox = IoC.Get<EntityComboBoxViewModel>();
            CheckedParents = new HashSet<EntityModel>();
        }
        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            await LoadEntities();
        }

        private async Task LoadEntities()
        {
            await ParentEntityComboBox.LoadEntities();
        }

        public EntityComboBoxViewModel ParentEntityComboBox
        {
            get { return _entityComboBox; }
            set
            {
                if (_entityComboBox != null)
                {
                    _entityComboBox.PropertyChanged -= OnParentEntityPropertyChanged;
                }
                _entityComboBox = value;
                if (_entityComboBox != null)
                {
                    _entityComboBox.PropertyChanged += OnParentEntityPropertyChanged;
                }

                NotifyOfPropertyChange(() => ParentEntityComboBox);
                NotifyOfPropertyChange(() => CanInsertParent);
            }
        }

        public EntityModel ChildEntity
        {
            get { return _childEntity; }
            set
            {
                _childEntity = value;
                NotifyOfPropertyChange(() => ChildEntity);
            }
        }

        public BindingList<EntityModel> ParentEntities
        {
            get { return _parentEntities; }
            set
            {
                _parentEntities = value;
                NotifyOfPropertyChange(() => ParentEntities);
            }
        }

        public HashSet<EntityModel> CheckedParents
        {
            get { return _checkedParents; }
            set
            {
                _checkedParents = value;
                NotifyOfPropertyChange(() => CheckedParents);
                NotifyOfPropertyChange(() => CanInsertParent);
            }
        }

        public void HandleChecked(EntityModel parent)
        {
            CheckedParents.Add(parent);
            NotifyOfPropertyChange(() => CanInsertParent);
        }

        public void HandleUnchecked(EntityModel parent)
        {
            CheckedParents.Remove(parent);
            NotifyOfPropertyChange(() => CanInsertParent);
        }

        public bool CanInsertParent
        {
            get
            {
                return ParentEntityComboBox.SelectedEntity != null && CheckedParents.Count > 0;
            }
        }

        public async Task InsertParent()
        {
            var insertParent = ParentEntityComboBox.SelectedEntity;
            if (insertParent == null)
            {
                throw new InvalidOperationException("Inserted parent entity must be selected");
            }
            
            // Relationship between child and new parent
            var relationship = new RelationshipModel(insertParent.Id, ChildEntity.Id);
            await _relationshipEndpoint.SaveRelationship(relationship);

            foreach (var parent in CheckedParents)
            {
                // Remove relationships between child and old parents
                var oldRelationship = new RelationshipModel(parent.Id, ChildEntity.Id);
                await _relationshipEndpoint.RemoveRelationshipByEntities(oldRelationship);

                // Insert relationship between new parent and old parents
                var newRelationship = new RelationshipModel(parent.Id, insertParent.Id);
                await _relationshipEndpoint.SaveRelationship(newRelationship);
            }

            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(ChildEntity.Id));
        }

        public void CancelInsert()
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(ChildEntity.Id));
        }

        public void Handle(InsertParentAboveEvent message)
        {
            ChildEntity = message.ChildEntity;
            ParentEntities = new BindingList<EntityModel>(message.ParentEntities);
        }

        private void OnParentEntityPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            NotifyOfPropertyChange(() => CanInsertParent);
        }
    }
}
