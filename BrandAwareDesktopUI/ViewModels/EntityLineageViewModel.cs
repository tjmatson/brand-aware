﻿using BrandAwareDesktopUI.EventModels;
using BrandAwareDesktopUI.Library.Api;
using BrandAwareDesktopUI.Library.Models;
using BrandAwareDesktopUI.Models;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BrandAwareDesktopUI.ViewModels
{
    public class EntityLineageViewModel : Screen, IHandle<EntityDetailEvent>
    {
        private IEntityEndpoint _entityEndpoint;
        private IEventAggregator _events;
        private BindingList<EntityItemModel> _parentRoots;
        private BindingList<EntityItemModel> _childRoots;

        public EntityLineageViewModel(IEntityEndpoint entityEndpoint, IEventAggregator events)
        {
            _entityEndpoint = entityEndpoint;
            _events = events;

            _events.Subscribe(this);
        }

        public GraphModel Graph { get; set; }
        public int EntityId { get; set; }

        public BindingList<EntityItemModel> ParentRoots
        {
            get { return _parentRoots; }
            set
            {
                _parentRoots = value;
                NotifyOfPropertyChange(() => ParentRoots);
            }
        }
        public BindingList<EntityItemModel> ChildRoots
        {
            get { return _childRoots; }
            set
            {
                _childRoots = value;
                NotifyOfPropertyChange(() => ChildRoots);
            }
        }

        public bool HasParents
        {
            get
            {
                List<int> parentAdj = null;
                Graph?.ParentRelationships.TryGetValue(EntityId.ToString(), out parentAdj);
                return parentAdj != null;
            }
        }

        public async void TreeOnSelectedItemChanged(RoutedPropertyChangedEventArgs<object> args)
        {
            var entityItem = (EntityItemModel)args.NewValue;
            if (entityItem != null)
            {
                await LoadLineage(entityItem.Entity.Id);
            }
        }

        // Actions

        public void AddChild()
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(AddChildViewModel)));

            Graph.Entities.TryGetValue(EntityId.ToString(), out EntityModel entity);
            _events.PublishOnUIThread(new AddChildEvent(entity));
        }

        public void AddParent()
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(AddParentViewModel)));

            Graph.Entities.TryGetValue(EntityId.ToString(), out EntityModel entity);
            _events.PublishOnUIThread(new AddParentEvent(entity));
        }

        public void RemoveParent()
        {
            var parents = GetRelatedEntities(EntityId, Graph.ParentRelationships);
            if (parents.Count > 0)
            {
                Graph.Entities.TryGetValue(EntityId.ToString(), out EntityModel entity);

                _events.PublishOnUIThread(new ChangeViewEvent(typeof(RemoveParentViewModel)));
                _events.PublishOnUIThread(new RemoveParentEvent(entity, parents));
            }
        }

        public void InsertParentAbove()
        {
            var parents = GetRelatedEntities(EntityId, Graph.ParentRelationships);
            Graph.Entities.TryGetValue(EntityId.ToString(), out EntityModel entity);

            _events.PublishOnUIThread(new ChangeViewEvent(typeof(InsertParentAboveViewModel)));
            _events.PublishOnUIThread(new InsertParentAboveEvent(entity, parents));
        }

        public List<EntityModel> GetRelatedEntities(int id, Dictionary<string, List<int>> adjacencies)
        {
            adjacencies.TryGetValue(id.ToString(), out List<int> adj);
            List<EntityModel> entities = new List<EntityModel>();
            if (adj.Count > 0)
            {
                foreach (var entityId in adj)
                {
                    Graph.Entities.TryGetValue(entityId.ToString(), out var entity);
                    entities.Add(entity);
                }
            }

            return entities;
        }

        /// <summary>
        /// Load lineage of given entity.
        /// </summary>
        /// <param name="message">Event message containing entity ID</param>
        public async void Handle(EntityDetailEvent message)
        {
            await LoadLineage(message.Id);
        }

        public async Task LoadLineage(int id)
        {
            EntityId = id;

            ParentRoots = new BindingList<EntityItemModel>();
            ChildRoots = new BindingList<EntityItemModel>();

            var graph = await _entityEndpoint.GetLineage(id);
            Graph = graph;

            ChildRoots.Add(GraphDFS(id, Graph, Graph.ChildRelationships));
            NotifyOfPropertyChange(() => ChildRoots);

            Graph.ParentRelationships.TryGetValue(id.ToString(), out List<int> parents);
            if (parents != null)
            {
                foreach (var parentId in parents)
                {
                    ParentRoots.Add(GraphDFS(parentId, Graph, Graph.ParentRelationships));
                }
                NotifyOfPropertyChange(() => ParentRoots);
            }
            NotifyOfPropertyChange(() => HasParents);
        }

        /// <summary>
        /// Create a recursively nested tree view of a directed acylcic graph.
        /// </summary>
        /// <param name="id">ID of the entity to start at</param>
        /// <param name="graph">Graph to traverse</param>
        /// <param name="relationships">Adjacency lists of relationships</param>
        /// <returns>An EntityItemModel with its neighbors nested recursively</returns>
        private EntityItemModel GraphDFS(int id, GraphModel graph, Dictionary<string, List<int>> relationships)
        {
            graph.Entities.TryGetValue(id.ToString(), out Library.Models.EntityModel entity);
            var item = new EntityItemModel(entity);
            relationships.TryGetValue(id.ToString(), out List<int> adj);

            if (adj != null)
            {
                foreach (var childId in adj)
                {
                    item.Entities.Add(GraphDFS(childId, graph, relationships));
                }
            }

            return item;
        }
    }
}
