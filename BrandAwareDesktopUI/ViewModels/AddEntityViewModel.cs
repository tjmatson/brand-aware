﻿using BrandAwareDesktopUI.EventModels;
using BrandAwareDesktopUI.Library.Api;
using BrandAwareDesktopUI.Library.Models;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.ViewModels
{
    public class AddEntityViewModel : Screen
    {
        private IEntityEndpoint _entityEndpoint;
        private IRelationshipEndpoint _relationshipEndpoint;
        private IEventAggregator _events;
        private EntityComboBoxViewModel _entityComboBox;
        private string _entityName;

        public AddEntityViewModel(IEntityEndpoint entityEndpoint, IRelationshipEndpoint relationshipEndpoint, IEventAggregator events)
        {
            _entityEndpoint = entityEndpoint;
            _relationshipEndpoint = relationshipEndpoint;
            _events = events;

            ParentEntityComboBox = IoC.Get<EntityComboBoxViewModel>();
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            await LoadEntities();
        }

        private async Task LoadEntities()
        {
            await ParentEntityComboBox.LoadEntities();
        }

        public EntityComboBoxViewModel ParentEntityComboBox
        {
            get { return _entityComboBox; }
            set
            {
                _entityComboBox = value;
                NotifyOfPropertyChange(() => ParentEntityComboBox);
            }
        }

        public string EntityName
        {
            get { return _entityName; }
            set
            {
                _entityName = value;
                NotifyOfPropertyChange(() => EntityName);
            }
        }

        public async Task AddEntity()
        {
            EntityModel entity = new EntityModel() { EntityName = EntityName };
            var entityId = await _entityEndpoint.PostEntity(entity);

            var parent = ParentEntityComboBox.SelectedEntity;
            if (parent != null)
            {
                var relationship = new RelationshipModel(parent.Id, entityId);
                await _relationshipEndpoint.SaveRelationship(relationship);
            }

            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(entityId));
        }

        public void CancelAdd()
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(SearchViewModel)));
        }
    }
}
