﻿using BrandAwareDesktopUI.EventModels;
using BrandAwareDesktopUI.Library.Api;
using BrandAwareDesktopUI.Library.Models;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.ViewModels
{
    public class EntityComboBoxViewModel : Screen
    {
        private BindingList<EntityModel> _results;
        private EntityModel _selectedEntity;
        private IEntityEndpoint _entityEndpoint;
        private IEventAggregator _events;

        public EntityComboBoxViewModel(IEntityEndpoint entityEndpoint, IEventAggregator events)
        {
            _entityEndpoint = entityEndpoint;
            _events = events;

            _events.Subscribe(this);
        }

        public BindingList<EntityModel> Results
        {
            get { return _results; }
            set
            {
                _results = value;
                NotifyOfPropertyChange(() => Results);
            }
        }

        public EntityModel SelectedEntity
        {
            get { return _selectedEntity; }
            set
            {
                _selectedEntity = value;
                NotifyOfPropertyChange(() => SelectedEntity);
            }
        }

        public async Task LoadEntities()
        {
            var entities = await _entityEndpoint.GetAll();
            Results = new BindingList<EntityModel>(entities);
        }
    }
}
