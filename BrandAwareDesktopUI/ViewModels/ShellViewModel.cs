﻿using BrandAwareDesktopUI.EventModels;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.ViewModels
{
    public class ShellViewModel : Conductor<object>, IHandle<ChangeViewEvent>
    {
        private IEventAggregator _events;

        public ShellViewModel(IEventAggregator events)
        {
            _events = events;

            _events.Subscribe(this);
            ActivateItem(IoC.Get<LoginViewModel>());
        }

        public void Handle(ChangeViewEvent message)
        {
            var instance = IoC.GetInstance(message.ViewModelType, null);
            ActivateItem(instance);
        }
    }
}
