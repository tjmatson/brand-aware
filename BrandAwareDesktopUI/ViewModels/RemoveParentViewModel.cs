﻿using BrandAwareDesktopUI.EventModels;
using BrandAwareDesktopUI.Library.Api;
using BrandAwareDesktopUI.Library.Models;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BrandAwareDesktopUI.ViewModels
{
    public class RemoveParentViewModel : Screen, IHandle<RemoveParentEvent>
    {
        private IEntityEndpoint _entityEndpoint;
        private IRelationshipEndpoint _relationshipEndpoint;
        private IEventAggregator _events;
        private BindingList<EntityModel> _parentEntities;
        private HashSet<EntityModel> _checkedParents;
        private EntityModel _entity;

        public RemoveParentViewModel(IEntityEndpoint entityEndpoint, IRelationshipEndpoint relationshipEndpoint, IEventAggregator events)
        {
            _entityEndpoint = entityEndpoint;
            _relationshipEndpoint = relationshipEndpoint;
            _events = events;

            _events.Subscribe(this);

            CheckedParents = new HashSet<EntityModel>();
        }

        public BindingList<EntityModel> ParentEntities
        {
            get { return _parentEntities; }
            set
            {
                _parentEntities = value;
                NotifyOfPropertyChange(() => ParentEntities);
            }
        }
        public HashSet<EntityModel> CheckedParents
        {
            get { return _checkedParents; }
            set
            {
                _checkedParents = value;
                NotifyOfPropertyChange(() => CheckedParents);
                NotifyOfPropertyChange(() => CanRemoveParents);
            }
        }

        public EntityModel Entity
        {
            get { return _entity; }
            set
            {
                _entity = value;
                NotifyOfPropertyChange(() => Entity);
            }
        }

        public void HandleChecked(EntityModel parent)
        {
            CheckedParents.Add(parent);
            NotifyOfPropertyChange(() => CanRemoveParents);
        }

        public void HandleUnchecked(EntityModel parent)
        {
            CheckedParents.Remove(parent);
            NotifyOfPropertyChange(() => CanRemoveParents);
        }

        public bool CanRemoveParents
        {
            get
            {
                return CheckedParents.Count > 0;
            }
        }

        public async void RemoveParents()
        {
            foreach (EntityModel parent in CheckedParents)
            {
                RelationshipModel relationship = new RelationshipModel(parent.Id, Entity.Id);
                await _relationshipEndpoint.RemoveRelationshipByEntities(relationship);
            }

            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(Entity.Id));
        }

        public void CancelRemove()
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(Entity.Id));
        }

        public void Handle(RemoveParentEvent message)
        {
            Entity = message.Entity;
            ParentEntities = new BindingList<EntityModel>(message.Parents);
        }
    }
}
