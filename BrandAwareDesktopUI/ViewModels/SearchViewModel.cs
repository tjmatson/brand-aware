﻿using BrandAwareDesktopUI.EventModels;
using BrandAwareDesktopUI.Library.Api;
using BrandAwareDesktopUI.Library.Models;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BrandAwareDesktopUI.ViewModels
{
    public class SearchViewModel : Screen
    {
        private BindingList<EntityModel> _results;
        private string _search;
        private IEntityEndpoint _entityEndpoint;
        private IEventAggregator _events;

        public SearchViewModel(IEntityEndpoint entityEndpoint, IEventAggregator events)
        {
            _entityEndpoint = entityEndpoint;
            _events = events;
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            await LoadEntities();
        }

        private async Task LoadEntities()
        {
            var entities = await _entityEndpoint.GetAll();
            Results = new BindingList<EntityModel>(entities);
        }

        public BindingList<EntityModel> Results
        {
            get { return _results; }
            set
            {
                _results = value;
                NotifyOfPropertyChange(() => Results);
            }
        }

        public string Search
        {
            get { return _search; }
            set
            {
                _search = value;
                NotifyOfPropertyChange(() => Search);
            }
        }

        public void OnSearchKeyDown(ActionExecutionContext executionContext)
        {
            var keyArgs = executionContext.EventArgs as KeyEventArgs;

            if (keyArgs != null && keyArgs.Key == Key.Enter)
            {
                NotifyOfPropertyChange(() => Results);
            }
        }

        public void OnEntityDoubleClick(EntityModel entity)
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(entity.Id));
        }

        public void AddEntity()
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(AddEntityViewModel)));
        }
    }
}
