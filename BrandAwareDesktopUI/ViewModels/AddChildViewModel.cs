﻿using BrandAwareDesktopUI.EventModels;
using BrandAwareDesktopUI.Library.Api;
using BrandAwareDesktopUI.Library.Models;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.ViewModels
{
    public class AddChildViewModel : Screen, IHandle<AddChildEvent>
    {
        private IEntityEndpoint _entityEndpoint;
        private IRelationshipEndpoint _relationshipEndpoint;
        private IEventAggregator _events;
        private string _childName = "";
        private EntityModel _parentEntity;

        public AddChildViewModel(IEntityEndpoint entityEndpoint, IRelationshipEndpoint relationshipEndpoint, IEventAggregator events)
        {
            _entityEndpoint = entityEndpoint;
            _relationshipEndpoint = relationshipEndpoint;
            _events = events;

            _events.Subscribe(this);
        }

        public EntityModel ParentEntity
        {
            get { return _parentEntity; }
            set
            {
                _parentEntity = value;
                NotifyOfPropertyChange(() => ParentEntity);
            }
        }

        public string ChildName {
            get { return _childName; }
            set
            {
                _childName = value;
                NotifyOfPropertyChange(() => ChildName);
                NotifyOfPropertyChange(() => CanAddChild);
            }
        }
        public bool CanAddChild
        {
            get
            {
                return ChildName.Length > 0;
            }
        }

        public async Task AddChild()
        {
            EntityModel child = new EntityModel() { EntityName = ChildName };
            var childId = await _entityEndpoint.PostEntity(child);

            var relationship = new RelationshipModel(ParentEntity.Id, childId);
            await _relationshipEndpoint.SaveRelationship(relationship);

            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(childId));
        }

        public void CancelAdd()
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(ParentEntity.Id));
        }

        public void Handle(AddChildEvent message)
        {
            ParentEntity = message.Parent;
        }
    }
}
