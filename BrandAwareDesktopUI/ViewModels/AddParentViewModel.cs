﻿using BrandAwareDesktopUI.EventModels;
using BrandAwareDesktopUI.Library.Api;
using BrandAwareDesktopUI.Library.Models;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.ViewModels
{
    public class AddParentViewModel : Screen, IHandle<AddParentEvent>
    {
        private IEntityEndpoint _entityEndpoint;
        private IRelationshipEndpoint _relationshipEndpoint;
        private IEventAggregator _events;
        private EntityModel _childEntity;
        private EntityComboBoxViewModel _entityComboBox;

        public AddParentViewModel(IEntityEndpoint entityEndpoint, IRelationshipEndpoint relationshipEndpoint, IEventAggregator events)
        {
            _entityEndpoint = entityEndpoint;
            _relationshipEndpoint = relationshipEndpoint;
            _events = events;

            _events.Subscribe(this);
            ParentEntityComboBox = IoC.Get<EntityComboBoxViewModel>();
        }

        protected override async void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            await LoadEntities();
        }

        private async Task LoadEntities()
        {
            await ParentEntityComboBox.LoadEntities();
        }

        public EntityComboBoxViewModel ParentEntityComboBox
        {
            get { return _entityComboBox; }
            set
            {
                if (_entityComboBox != null)
                {
                    _entityComboBox.PropertyChanged -= OnParentEntityPropertyChanged;
                }
                _entityComboBox = value;
                if (_entityComboBox != null)
                {
                    _entityComboBox.PropertyChanged += OnParentEntityPropertyChanged;
                }

                NotifyOfPropertyChange(() => ParentEntityComboBox);
                NotifyOfPropertyChange(() => CanAddParent);
            }
        }

        public EntityModel ChildEntity
        {
            get { return _childEntity; }
            set
            {
                _childEntity = value;
                NotifyOfPropertyChange(() => ChildEntity);
            }
        }

        public bool CanAddParent
        {
            get
            {
                return ParentEntityComboBox.SelectedEntity != null;
            }
        }

        public async Task AddParent()
        {
            var parentEntity = ParentEntityComboBox.SelectedEntity;
            if (parentEntity == null)
            {
                throw new InvalidOperationException("Parent must be selected");
            }
            var relationship = new RelationshipModel(parentEntity.Id, ChildEntity.Id);
            await _relationshipEndpoint.SaveRelationship(relationship);

            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(ChildEntity.Id));
        }

        public void CancelAdd()
        {
            _events.PublishOnUIThread(new ChangeViewEvent(typeof(EntityLineageViewModel)));
            _events.PublishOnUIThread(new EntityDetailEvent(ChildEntity.Id));
        }

        public void Handle(AddParentEvent message)
        {
            ChildEntity = message.ChildEntity;
        }

        private void OnParentEntityPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            NotifyOfPropertyChange(() => CanAddParent);
        }
    }
}