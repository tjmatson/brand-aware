﻿using Autofac.Extras.Moq;
using BrandAwareAPI.Library.DataAccess;
using BrandAwareAPI.Library.Internal.DataAccess;
using BrandAwareAPI.Library.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrandAware.Tests
{
    public class RelationshipDataTests
    {
        [Theory]
        [InlineData(0, 2)]
        [InlineData(0, 8)]
        [InlineData(0, 6)]
        [InlineData(0, 3)]
        public void CreatesCycle_ShouldCreateCycle(int childId, int parentId)
        {
            var entity = GetSampleChildEntities()[0];
            var sqlChildren = "dbo.spEntityChildren";
            var sqlParents = "dbo.spEntityParents";
            var db = "BrandAwareData";

            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.LoadData<ChildParentModel, object>(sqlChildren, It.IsAny<object>(), db))
                    .Returns(GetSampleChildEntities());

                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.LoadData<ChildParentModel, object>(sqlParents, It.IsAny<object>(), db))
                    .Returns(GetSampleParentEntities());

                var cls = mock.Create<RelationshipData>();

                var expected = true;

                RelationshipModel relationship = new RelationshipModel()
                {
                    Id = 0,
                    ChildId = childId,
                    ParentId = parentId
                };

                var actual = cls.CreatesCycle(relationship);

                Assert.Equal(expected, actual);
            }
        }

        [Fact]
        public void CreatesCycle_ShouldNotCreateCycle()
        {
            var entity = GetSampleChildEntities()[0];
            var sqlChildren = "dbo.spEntityChildren";
            var sqlParents = "dbo.spEntityParents";
            var db = "BrandAwareData";
            var childId = 0;
            var parentId = 9;

            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.LoadData<ChildParentModel, object>(sqlChildren, It.IsAny<object>(), db))
                    .Returns(GetSampleChildEntities());

                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.LoadData<ChildParentModel, object>(sqlParents, It.IsAny<object>(), db))
                    .Returns(GetSampleParentEntities());

                var cls = mock.Create<RelationshipData>();

                var expected = false;

                RelationshipModel relationship = new RelationshipModel() {
                    Id = 0,
                    ChildId = childId,
                    ParentId = parentId
                };

                var actual = cls.CreatesCycle(relationship);

                Assert.Equal(expected, actual);
            }
        }

        /// <summary>
        /// Returns children of entity 0
        /// 
        /// The structure going from parents to children is:
        /// 
        /// 0 -> 1 -> 2 -> 3
        ///   -> 4
        /// </summary>
        /// <returns>List of entities with their relationships</returns>
        private List<ChildParentModel> GetSampleChildEntities()
        {
            List<ChildParentModel> output = new List<ChildParentModel>();

            output.Add(new ChildParentModel(1, "Entity1", "Sector1", 0, "Entity0", "Sector0"));
            output.Add(new ChildParentModel(2, "Entity2", "Sector2", 1, "Entity1", "Sector1"));
            output.Add(new ChildParentModel(3, "Entity3", "Sector3", 2, "Entity2", "Sector2"));
            output.Add(new ChildParentModel(4, "Entity4", "Sector4", 0, "Entity0", "Sector0"));

            return output;
        }

        /// <summary>
        /// Returns parents of entity 0
        /// 
        /// The structure going from children to parents is:
        /// 
        /// 0 -> 5 -> 6
        ///        -> 7 -> 8
        /// </summary>
        /// <returns>List of entities with their relationships</returns>
        private List<ChildParentModel> GetSampleParentEntities()
        {
            List<ChildParentModel> output = new List<ChildParentModel>();

            output.Add(new ChildParentModel(0, "Entity0", "Sector0", 5, "Entity5", "Sector5"));
            output.Add(new ChildParentModel(5, "Entity5", "Sector5", 6, "Entity6", "Sector6"));
            output.Add(new ChildParentModel(5, "Entity5", "Sector5", 7, "Entity7", "Sector7"));
            output.Add(new ChildParentModel(7, "Entity7", "Sector7", 8, "Entity8", "Sector8"));

            return output;
        }
    }
}
