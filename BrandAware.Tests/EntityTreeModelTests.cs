﻿using BrandAwareAPI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Extensions;

namespace BrandAware.Tests
{
    public class EntityTreeModelTests
    {
        [Fact]
        public void GetRoot_ShouldSetAndGetRoot()
        {
            EntityTreeModel tree = new EntityTreeModel();

            EntityModel root = new EntityModel() { Id = 0, EntityName = "Root" };
            tree.SetRoot(root);
            
            EntityNodeModel actual = tree.GetRoot();

            Assert.Equal(root.Id, actual.Id);
            Assert.Equal(root.EntityName, actual.Name);
        }

        [Fact]
        public void AddEntities_ShouldSkipDuplicates()
        {
            var entityPairs = GetSampleEntityPairs();
            HashSet<int> uniqueIds = new HashSet<int>();

            EntityTreeModel tree = new EntityTreeModel();

            foreach(var pair in entityPairs)
            {
                uniqueIds.Add(pair.Item1.Id);
                uniqueIds.Add(pair.Item2.Id);

                tree.AddEntities(pair.Item1, pair.Item2);
            }

            var expected = uniqueIds.Count;

            var actual = tree.Nodes.Count;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AddEntities_ShouldStructureCorrectly()
        {
            var entityPairs = GetSampleEntityPairs();

            EntityTreeModel tree = new EntityTreeModel();

            foreach (var pair in entityPairs)
            {
                tree.AddEntities(pair.Item1, pair.Item2);
            }

            foreach (var pair in entityPairs)
            {
                tree.Nodes.TryGetValue(pair.Item1.Id, out EntityNodeModel child);
                tree.Nodes.TryGetValue(pair.Item2.Id, out EntityNodeModel parent);

                Assert.Contains(child, parent.Nodes);
            }
        }

        [Fact]
        public void AddEntities_ShouldHaveNodeCount()
        {
            var entityPairs = GetSampleEntityPairs();

            EntityTreeModel tree = new EntityTreeModel();

            foreach (var pair in entityPairs)
            {
                tree.AddEntities(pair.Item1, pair.Item2);
            }

            EntityNodeModel entity;

            tree.Nodes.TryGetValue(0, out entity);
            Assert.Empty(entity.Nodes);

            tree.Nodes.TryGetValue(1, out entity);
            Assert.Equal(2, entity.Nodes.Count);

            tree.Nodes.TryGetValue(2, out entity);
            Assert.Single(entity.Nodes);

            tree.Nodes.TryGetValue(3, out entity);
            Assert.Single(entity.Nodes);

            tree.Nodes.TryGetValue(4, out entity);
            Assert.Empty(entity.Nodes);
        }

        /// <summary>
        /// Get a list of sample child and parent entity pairs.
        /// </summary>
        /// <returns>A list of tuples where Item1 is the child and Item2 is the parent</returns>
        public static List<(EntityModel, EntityModel)> GetSampleEntityPairs()
        {
            List<(EntityModel, EntityModel)> entities = new List<(EntityModel, EntityModel)>();

            entities.Add((
                new EntityModel() { Id = 0, EntityName = "Test0" },
                new EntityModel() { Id = 1, EntityName = "Test1" })
            );
            entities.Add((
                new EntityModel() { Id = 1, EntityName = "Test1" },
                new EntityModel() { Id = 2, EntityName = "Test2" })
            );
            entities.Add((
                new EntityModel() { Id = 2, EntityName = "Test2" },
                new EntityModel() { Id = 3, EntityName = "Test3" })
            );
            entities.Add((
                new EntityModel() { Id = 4, EntityName = "Test1" },
                new EntityModel() { Id = 1, EntityName = "Test4" })
            );

            return entities;
        }
    }
}
