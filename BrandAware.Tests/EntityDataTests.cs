﻿using Autofac.Extras.Moq;
using BrandAwareAPI.Library.DataAccess;
using BrandAwareAPI.Library.Internal.DataAccess;
using BrandAwareAPI.Library.Models;
using Dapper;
using Moq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BrandAware.Tests
{
    public class EntityDataTests
    {
        [Fact]
        public void GetEntities_ShouldGetAllEntities()
        {
            var sql = "dbo.spEntity_All";
            var db = "BrandAwareData";

            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.LoadData<EntityModel, object>(sql, It.IsAny<object>(), db))
                    .Returns(GetSampleEntities());

                var cls = mock.Create<EntityData>();
                var expected = GetSampleEntities();

                var actual = cls.GetEntities();

                Assert.True(actual != null);
                Assert.Equal(expected.Count, actual.Count);

                foreach (var (e, a) in expected.Zip(actual, Tuple.Create))
                {
                    Assert.Equal(e.Id, a.Id);
                    Assert.Equal(e.EntityName, a.EntityName);
                }
            }
        }

        [Fact]
        public void SaveEntity_ShouldSaveEntity()
        {
            var entity = GetSampleEntities()[0];
            var sql = "dbo.spEntity_Insert";
            var db = "BrandAwareData";

            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.SaveData(sql, It.IsAny<DynamicParameters>(), db));

                var cls = mock.Create<EntityData>();

                // DynamicParameter isn't being mocked so check that it's throwing an exception for now
                Assert.Throws<NullReferenceException>(() => cls.SaveEntity(entity));

                mock.Mock<ISqlDataAccess>()
                    .Verify(x => x.SaveData(sql, It.IsAny<DynamicParameters>(), db), Times.Exactly(1));
            }
        }

        [Fact]
        public void UpdateEntity_ShouldUpdateEntity()
        {
            var entity = GetSampleEntities()[0];
            var sql = "dbo.spEntity_Update";
            var db = "BrandAwareData";

            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.SaveData(sql, entity, db));

                var cls = mock.Create<EntityData>();

                cls.UpdateEntity(entity);

                mock.Mock<ISqlDataAccess>()
                    .Verify(x => x.SaveData(sql, entity, db), Times.Exactly(1));
            }
        }

        [Fact]
        public void RemoveEntity_ShouldRemoveEntityAndAdoptChildren()
        {
            var entityId = 1;
            var orphanChildren = false;
            var db = "BrandAwareData";
            var sql = "dbo.spEntity_Remove";
            var sqlParents = "dbo.spEntity_DirectParents";
            var sqlChildren = "dbo.spEntity_DirectChildren";
            var sqlInsert = "dbo.spRelationship_Insert";

            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.SaveData(sql, It.IsAny<object>(), db));

                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.LoadDataInTransaction<EntityModel, object>(sqlParents, It.IsAny<object>()))
                    .Returns(GetSampleParents());

                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.LoadDataInTransaction<EntityModel, object>(sqlChildren, It.IsAny<object>()))
                    .Returns(GetSampleChildren());

                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.SaveDataInTransaction(sqlInsert, It.IsAny<object>()));

                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.CommitTransaction());

                var cls = mock.Create<EntityData>();

                cls.RemoveEntity(entityId, orphanChildren);

                mock.Mock<ISqlDataAccess>()
                    .Verify(x => x.SaveData(sql, It.IsAny<object>(), db), Times.Exactly(0));

                mock.Mock<ISqlDataAccess>()
                    .Verify(x => x.LoadDataInTransaction<EntityModel, object>(sqlParents, It.IsAny<object>()), Times.Exactly(1));

                mock.Mock<ISqlDataAccess>()
                    .Verify(x => x.LoadDataInTransaction<EntityModel, object>(sqlChildren, It.IsAny<object>()), Times.Exactly(1));

                mock.Mock<ISqlDataAccess>()
                    .Verify(x => x.SaveDataInTransaction(sqlInsert, It.IsAny<object>()), Times.Exactly(4));

                mock.Mock<ISqlDataAccess>()
                    .Verify(x => x.Dispose(), Times.Exactly(1));
            }
        }

        [Fact]
        public void RemoveEntity_ShouldRemoveEntityAndOrphanChildren()
        {
            var entityId = 1;
            var orphanChildren = true;
            var db = "BrandAwareData";
            var sql = "dbo.spEntity_Remove";

            using (var mock = AutoMock.GetLoose())
            {
                mock.Mock<ISqlDataAccess>()
                    .Setup(x => x.SaveData(sql, It.IsAny<object>(), db));

                var cls = mock.Create<EntityData>();

                cls.RemoveEntity(entityId, orphanChildren);

                mock.Mock<ISqlDataAccess>()
                    .Verify(x => x.SaveData(sql, It.IsAny<object>(), db), Times.Exactly(1));
            }
        }

        private List<EntityModel> GetSampleEntities()
        {
            List<EntityModel> output = new List<EntityModel>
            {
                new EntityModel() { Id = 0, EntityName = "Unilever" },
                new EntityModel() { Id = 1, EntityName = "Nestle" },
                new EntityModel() { Id = 2, EntityName = "Kellogs" },
                new EntityModel() { Id = 3, EntityName = "PepsiCo" },
                new EntityModel() { Id = 4, EntityName = "Lays" }
            };
            return output;
        }

        private List<EntityModel> GetSampleParents()
        {
            List<EntityModel> output = new List<EntityModel>
            {
                new EntityModel() { Id = 0, EntityName = "Unilever" },
                new EntityModel() { Id = 1, EntityName = "Nestle" }
            };
            return output;
        }

        private List<EntityModel> GetSampleChildren()
        {
            List<EntityModel> output = new List<EntityModel>
            {
                new EntityModel() { Id = 3, EntityName = "PepsiCo" },
                new EntityModel() { Id = 4, EntityName = "Lays" }
            };
            return output;
        }

        [Theory]
        [InlineData("John's pizza", "Johns pizza")]
        [InlineData("Thirty-first", "Thirty first")]
        [InlineData("'-''--", "   ")]
        [InlineData("", "")]
        [InlineData("âëíòñõā-'", "aeionoa ")]
        public void SearchNormalize_ShouldRemoveCharactersAndDiacritics(string input, string expected)
        {
            using (var mock = AutoMock.GetLoose())
            {
                var cls = mock.Create<EntityData>();
                string actual = cls.SearchNormalize(input);

                Assert.Equal(expected, actual);
            }
        }


        [Theory]
        [InlineData("apple pie", "\"apple*\" AND \"pie*\"")]
        [InlineData("orange", "\"orange*\"")]
        [InlineData("", "")]
        [InlineData("   joe   rogers  ", "\"joe*\" AND \"rogers*\"")]
        public void QueryToMultiPrefix_ShouldConvertQueryToMultiPrefix(string query, string expected)
        {
            using (var mock = AutoMock.GetLoose())
            {
                var cls = mock.Create<EntityData>();
                string actual = cls.QueryToMultiPrefix(query);

                Assert.Equal(expected, actual);
            }
        }
    }
}
