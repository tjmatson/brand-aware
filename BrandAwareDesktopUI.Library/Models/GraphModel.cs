﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.Library.Models
{
    public class GraphModel
    {
        public Dictionary<string, EntityModel> Entities { get; set; }
        public Dictionary<string, List<int>> ChildRelationships { get; set; }
        public Dictionary<string, List<int>> ParentRelationships { get; set; }
    }
}
