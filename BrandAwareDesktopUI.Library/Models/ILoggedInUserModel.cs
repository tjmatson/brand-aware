﻿namespace BrandAwareDesktopUI.Library.Models
{
    public interface ILoggedInUserModel
    {
        string EmailAddress { get; set; }
        string Id { get; set; }
        string Token { get; set; }
    }
}