﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.Library.Models
{
    public class EntityModel
    {
        public int Id { get; set; }
        public string EntityName { get; set; }
    }
}
