﻿using BrandAwareDesktopUI.Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.Library.Api
{
    public class RelationshipEndpoint : IRelationshipEndpoint
    {
        private IAPIHelper _apiHelper;

        public RelationshipEndpoint(IAPIHelper apiHelper)
        {
            _apiHelper = apiHelper;
        }

        public async Task SaveRelationship(RelationshipModel relationship)
        {
            using (HttpResponseMessage response = await _apiHelper.ApiClient.PostAsJsonAsync("api/Relationship", relationship))
            {
                if (response.IsSuccessStatusCode)
                {
                    // Do nothing
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }

        public async Task RemoveRelationshipByEntities(RelationshipModel relationship)
        {
            using (HttpResponseMessage response = await _apiHelper.ApiClient.PostAsJsonAsync("api/Relationship/DeleteByEntities", relationship))
            {
                if (response.IsSuccessStatusCode)
                {
                    // Do nothing
                }
                else
                {
                    throw new Exception(response.ReasonPhrase);
                }
            }
        }
    }
}
