﻿using BrandAwareDesktopUI.Library.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.Library.Api
{
    public interface IEntityEndpoint
    {
        Task<List<EntityModel>> GetAll();
        Task<GraphModel> GetLineage(int id);
        Task<int> PostEntity(EntityModel entity);
    }
}