﻿using BrandAwareDesktopUI.Library.Models;
using System.Threading.Tasks;

namespace BrandAwareDesktopUI.Library.Api
{
    public interface IRelationshipEndpoint
    {
        Task SaveRelationship(RelationshipModel relationship);
        Task RemoveRelationshipByEntities(RelationshipModel relationship);
    }
}