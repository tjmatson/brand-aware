// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Axios from 'axios'
import qs from 'qs'

import BootstrapVue from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import MultiSelect from 'vue-multiselect'
Vue.component('multiselect', MultiSelect);

Vue.config.productionTip = false;

Vue.prototype.$http = Axios;
Vue.prototype.$qs = qs;

Vue.use(BootstrapVue);

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App }
});
