import Vue from 'vue'
import Vuex from 'vuex'

import { account } from './account.module'
import { alert } from './alert.module'
import { entity } from './entity.module'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        account,
        alert,
        entity
    }
});

export default store