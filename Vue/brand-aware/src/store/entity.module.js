import Axios from 'axios'
import qs from 'qs'
import { errorMeaning } from '@/helpers/http'

const defaultEntity = {Id: -1, Name: "Loading", Nodes: []};

const state = {
    root: defaultEntity,
    children: [],
    loadFailed: false,
    loaded: false
}

const actions = {
    loadEntityData({ dispatch, commit }, { id }) {
        commit("setLoadFailed", false);
        commit("setLoaded", false);

        let loadRoot = Axios.get(`https://localhost:44320/api/entity/Parents/${id}`);
        let loadChildren = Axios.get(`https://localhost:44320/api/entity/DirectChildren/${id}`);

        Promise.all([loadRoot, loadChildren])
        .then(responses => {
            commit("setRoot", responses[0].data);
            commit("setChildren", responses[1].data);
            commit("setLoaded", true);
        }).catch(error => {
            dispatch("alert/error", `Unable to load brand data - ${errorMeaning(error)}`, { root: true });
            commit("setLoadFailed", true);
        });
    },

    getLazyLoadEntity(context, { id }) {
        return Axios.get(`https://localhost:44320/api/entity/LazyLoad/${id}`);
    },

    getChildren(context, { id }) {
        return Axios.get(`https://localhost:44320/api/entity/Children/${id}`);
    },

    getDirectChildren(context, { id }) {
        return Axios.get(`https://localhost:44320/api/entity/DirectChildren/${id}`);
    },

    search(context, { query, page, fetch }) {
        return Axios({
            method: 'post',
            url: "https://localhost:44320/api/entity/Search",
            data: qs.stringify({
                Query: query,
                Page: page,
                Fetch: fetch
            }),
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            }
        });
    },

    createEntity(context, { name, sector }) {
        return Axios({
            method: "post",
            url: "https://localhost:44320/api/entity",
            data: qs.stringify({
                Name: name,
                Sector: sector
            }),
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
    },

    deleteEntity(context, { id, orphanChildren }) {
        return Axios({
            method: "post",
            url: "https://localhost:44320/api/entity/DeleteEntity",
            data: qs.stringify({
                Id: id,
                OrphanChildren: orphanChildren
            }),
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
    },

    changeName(context, { id, name }) {
        return Axios({
            method: "post",
            url: `https://localhost:44320/api/entity/ChangeName/${id}`,
            data: `"${name}"`,
            headers: {
                'content-type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
    },

    changeSector(context, { id, sector }) {
        return Axios({
            method: "post",
            url: `https://localhost:44320/api/entity/ChangeSector/${id}`,
            data: `"${sector}"`,
            headers: {
                'content-type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
    },

    createChild(context, { parentId, childName, childSector }) {
        return Axios({
            method: 'post',
            url: `https://localhost:44320/api/entity/CreateChild/${parentId}`,
            data: qs.stringify({
                Name: childName,
                Sector: childSector
            }),
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
    },

    createConnection(context, { parentId, childId }) {
        return Axios({
            method: 'post',
            url: "https://localhost:44320/api/relationship/",
            data: qs.stringify({
                ParentId: parentId,
                ChildId: childId
            }),
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
    },

    removeConnection(context, { parentId, childId }) {
        return Axios({
            method: 'post',
            url: 'https://localhost:44320/api/relationship/DeleteByEntities',
            data: qs.stringify({
                ParentId: parentId,
                ChildId: childId
            }),
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        });
    },

    resetEntity({ commit }) {
        commit("setRoot", defaultEntity);
        commit("setChildren", []);
    }
}

const mutations = {
    setRoot(state, root) {
        state.root = root;
    },

    setChildren(state, children) {
        state.children = children;
    },

    setLoaded(state, loaded) {
        state.loaded = loaded;
    },

    setLoadFailed(state, loadFailed) {
        state.loadFailed = loadFailed;
    }
}

export const entity = {
    namespaced: true,
    state,
    actions,
    mutations
}