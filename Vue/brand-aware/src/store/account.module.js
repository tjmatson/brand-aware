import Axios from 'axios'
import qs from 'qs'

import router from '@/router'

const state = {
    loggedIn: (localStorage.getItem("token") ? true : false)
}

const actions = {
    login({ commit, dispatch, state }, { email, password }) {
        return Axios({
            method: 'post',
            url: 'https://localhost:44320/Token',
            data: qs.stringify({
                grant_type: "password",
                username: email,
                password: password,
            }),
            headers: {
                'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
        }).then(response => {
            localStorage.setItem("token", response.data.access_token);
            commit("updateLoggedIn")

            if (state.loggedIn) {
                router.push({ name: "home" });
            }
        }).catch(error => {
            dispatch("alert/error", "Unable to log in", { root: true });
        });
    },

    async register({ dispatch, state }, { email, password, confirmPassword }) {
        Axios.post('https://localhost:44320/api/Account/Register', {
            Email: email,
            Password: password,
            ConfirmPassword: confirmPassword
        }).then(async response => {
            await dispatch("login", { email, password });
            if (state.loggedIn) {
                router.push({ name: 'home' });
            } else {
                router.push({ name: 'login' });
            }
        }).catch(error => {
            dispatch("alert/error", "Unable to register", { root: true });
        });
    },

    logout({ commit }) {
        localStorage.removeItem("token");
        router.push({ name: "home" })
        commit("updateLoggedIn")
    }
}

const mutations = {
    updateLoggedIn(state) {
        state.loggedIn = (localStorage.getItem("token") ? true : false)
    }
}

export const account = {
    namespaced: true,
    state,
    actions,
    mutations
}