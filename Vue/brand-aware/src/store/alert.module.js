const state = {
    type: null,
    showError: false,
    message: null,
    maxSeconds: 4,
    successCountdown: 0
}

const actions = {
    success({ commit }, message) {
        commit("success", message);
    },

    error({ commit }, message) {
        commit("error", message);
    },

    clear({ commit }) {
        commit("clear");
    }
}

const mutations = {
    success(state, message) {
        state.type = "alert-success";
        state.message = message;
        state.successCountdown = state.maxSeconds;
        state.showError = false;
    },

    error(state, message) {
        state.type = "alert-error";
        state.message = message;
        state.showError = true;
        state.successCountdown = 0;
    },

    clear(state) {
        state.type = null;
        state.message = null;
        state.showError = false;
        state.successCountdown = 0;
    }
}

export const alert = {
    namespaced: true,
    state,
    actions,
    mutations
}