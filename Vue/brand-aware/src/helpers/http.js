export function errorMeaning(error) {
    if (error.response) {
        return `HTTP ${error.response.status}`;
    } else if (error.request) {
        return "Unable to connect";
    } else {
        console.error(error);
        return "Unknown error";
    }
}